﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InsiderList.Models
{

    public enum ConfirmationStatus
    {
        Unknown, Confirmed, Rejected
    }

    public class ConfirmationHistory
    {
        public int Id { get; set; }

        public string ConfirmationId { get; set; }
        public Confirmation Confirmation { get; set; }

        [Required]
        public DateTime EffectiveFrom { get; set; }
        [Required]
        public DateTime EffectiveTo { get; set; }
        [Required]
        public ConfirmationStatus Status { get; set; }

        public DateTime? Time { get; set; }

        public string Ip { get; set; }
    }
}