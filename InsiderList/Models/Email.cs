﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InsiderList.Models
{
    public enum EmailStatus
    {
        Created,Sent
    }

    public class Email
    {
        public Email()
        {
            Id = Guid.NewGuid().ToString();
        }
        public string Id { get; set; }

        [Required]
        [Index("IX_FirstAndSecond", IsUnique = true)]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Email subject")]
        public string EmailSubject { get; set; }
        [Required]
        [Display(Name = "Email text")]
        [AllowHtml]
        public string EmailText { get; set; }
        [Display(Name = "Email footer")]
        [Required]
        public string EmailFooter { get; set; }
        [Display(Name = "Confirmation text")]
        [Required]
        [AllowHtml]
        public string ConfirmationText { get; set; }

        [Required]
        public EmailStatus Status { get; set; }

        public string InsiderCreatedId {get;set;}
        [Display(Name = "Created by")]
        public Insider InsiderCreated { get; set; }
        
        public string InsiderSendedId { get; set; }
        [Display(Name = "Sended by")]
        public Insider InsiderSended { get; set; }

        [Display(Name = "Date sended")]
        public DateTime? DateSended { get; set; }

        public List<Confirmation> Confirmations { get; set; }

    }
}