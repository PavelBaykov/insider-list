﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InsiderList.Models
{
    
    public class Confirmation
    {
        public Confirmation()
        {
            Id = Guid.NewGuid().ToString();
        }
        public string Id { get; set; }

        [Required]
        public string InsiderId { get; set; }
        public Insider Insider { get; set; }

        [Required]
        public string EmailId { get; set; }
        public Email Email { get; set; }

        public List<ConfirmationHistory> ConfirmationHistory { get; set; }

        
    }
}