﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace InsiderList.Models
{
    public enum Role
    {
        Insider, Assistant
    }

    public class Insider
    {
        public Insider()
        {
            Id = Guid.NewGuid().ToString();
        }

        public string Id { get; set; }

        public List<Email> EmailsCreated { get; set; }
        public List<Email> EmailsSended { get; set; }
        public List<Confirmation> Confirmations { get; set; }

        [Required]
        [Index(IsUnique = true)]
        [StringLength(50, MinimumLength = 1)]
        public string Login { get; set; }

        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Surname")]
        public string LastName { get; set; }

        [Display(Name = "Birth surname")]
        public string BirthSurname { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        
        [Required]
        public string CategoryId { get; set; }
        public Category Category { get; set; }

        [Required]
        public Role Role { get; set; }

        [Required]
        [Display(Name = "Company name and address")]
        [StringLength(200, MinimumLength = 1)]
        public string CompanyNameAndAddress { get; set; }
        
        [Display(Name = "Function")]
        [StringLength(500, MinimumLength = 1)]
        public string Function { get; set; }

        [Display(Name = "Reason for being insider")]
        [StringLength(500, MinimumLength = 1)]
        public string Reason { get; set; }

        [Display(Name = "National identification number (if applicable)")]
        public string NationalIdentificationNumber { get; set; }

        [Display(Name = "Work direct telephone number")]
        public string WorkDirectTelephoneNumber { get; set; }

        [Display(Name = "Work mobile number")]
        public string WorkMobileNumber { get; set; }

        [Display(Name = "Home telephone number")]
        public string HomeTelephoneNumber { get; set; }

        [Display(Name = "Home mobile telephone number")]
        public string HomeMobileTelephoneNumber { get; set; }

        [Display(Name = "Date of birth")]
        public DateTime? DateOfBirth { get; set; }

        [Display(Name = "Country")]
        public string AddressCountry { get; set; }
        [Display(Name = "City")]
        public string AddressCity { get; set; }
        [Display(Name = "Post code")]
        public string PostCode { get; set; }
        [Display(Name = "Street name")]
        public string AddressStreetName { get; set; }
        [Display(Name = "Street number")]
        public string AddressStreetNumber { get; set; }

        [Display(Name = "User registered")]
        public DateTime? UserRegistered { get; set; }

        [Display(Name = "User updated")]
        public DateTime? UserUpdated { get; set; }

        [Display(Name = "Obtained")]
        public DateTime? Obtained { get; set; }

        [Display(Name = "Ceased")]
        public DateTime? Ceased { get; set; }

    }
}