﻿using InsiderList.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace InsiderList.DAL
{
    public class InsidersContext : DbContext
    {
        public InsidersContext() : base("InsidersConnection")
        {
        }

        public DbSet<Insider> Insiders{get;set;}
        public DbSet<Category> Categories { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<Confirmation> Confirmations { get; set; }
        public DbSet<ConfirmationHistory> ConfirmationsHistory { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasMany(c => c.Insiders)
                .WithRequired(i => i.Category)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<Insider>()
                .HasMany(ins => ins.EmailsCreated)
                .WithOptional(e => e.InsiderCreated)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<Insider>()
                .HasMany(ins => ins.EmailsSended)
                .WithOptional(e => e.InsiderSended)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<Insider>()
                .HasMany(ins => ins.Confirmations)
                .WithRequired(conf => conf.Insider)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<Email>()
                .HasMany(e => e.Confirmations)
                .WithRequired(conf => conf.Email)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<Confirmation>()
               .HasMany(conf => conf.ConfirmationHistory)
               .WithRequired(hist => hist.Confirmation)
               .WillCascadeOnDelete(true);
        }
    }
}