﻿using InsiderList.DAL;
using InsiderList.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace InsiderList
{
    public class CustomRoleProvider : RoleProvider
    {
        private InsidersContext _context = new InsidersContext();

        public override bool IsUserInRole(string username, string roleName)
        {
            var user = username.Substring(username.LastIndexOf('\\') + 1);

            if (roleName == "Admin")
            {
                if (user.ToLower() == System.Configuration.ConfigurationManager.AppSettings["AdminLogin"])
                    return true;
            }
            else if (roleName == "Insider" || roleName == "Assistant")
            {
                var userFromDb = _context.Insiders
                    .FirstOrDefault(ins => ins.Login.ToLower() == user);

                if (userFromDb != null)
                {
                    if (roleName == "Insider" && userFromDb.Role == Role.Insider)
                        return true;

                    if (roleName == "Assistant" && userFromDb.Role == Role.Assistant)
                        return true;
                }
            }

            return false;

        }

        public override string[] GetRolesForUser(string username)
        {
            var user = username.Substring(username.LastIndexOf('\\') + 1);

            if (user.ToLower() == System.Configuration.ConfigurationManager.AppSettings["AdminLogin"])
            {
                return new string[1] { "Admin" };
            }

            var userFromDb = _context.Insiders
                .FirstOrDefault(ins => ins.Login.ToLower() == user);

            if (userFromDb != null)
            {
                if (userFromDb.Role == Role.Assistant)
                {
                    return new string[] { "Assistant" };
                }
                else
                {
                    return new string[] { "Insider" };
                }
            }
            return new string[] { };
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }



        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }



        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}