﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InsiderList.DAL;
using InsiderList.Models;

namespace InsiderList.Controllers
{
    [Authorize(Roles = "Admin,Assistant")]
    public class InsidersController : Controller
    {
        private InsidersContext _db = new InsidersContext();

        // GET: Insiders
        public ActionResult InsidersList()
        {
            var insiders = _db.Insiders
                    .Include(i => i.Category)
                    .ToList();

            return View(insiders);
        }

        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Insider insider = _db.Insiders
                .Include(ins => ins.Category)
                .FirstOrDefault(ins => ins.Id == id);
            if (insider == null)
            {
                return HttpNotFound();
            }
            return View(insider);
        }

        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(_db.Categories, "Id", "CategoryName");
            var user = User.Identity.Name.Substring(User.Identity.Name.LastIndexOf('\\') + 1);

            return View();
        }

        // POST: Insiders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Login,FirstName,LastName,BirthSurname,Email,Role,CategoryId,CompanyNameAndAddress,Function,DateOfBirth,SystemId,Reason,Ceased,Obtained,WorkDirectTelephoneNumber,WorkMobileNumber,HomeTelephoneNumber,HomeMobileTelephoneNumber,AddressCountry,AddressCity,PostCode,AddressStreetName,AddressStreetNumber,NationalIdentificationNumber")] Insider insider)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    insider.UserRegistered = DateTime.UtcNow;
                    _db.Insiders.Add(insider);
                    _db.SaveChanges();
                    return RedirectToAction("InsidersList");
                }

                ViewBag.CategoryId = new SelectList(_db.Categories, "Id", "CategoryName", insider.CategoryId);
                return View(insider);
            }
            catch
            {
                var loginFromDb = _db.Insiders
                        .FirstOrDefault(ins => ins.Login.ToLower() == insider.Login.ToLower() && ins.Id != insider.Id);

                if (loginFromDb != null)
                {
                    ModelState.AddModelError("", "Error happened : the user with current login already exists");
                }
                else
                {
                    ModelState.AddModelError("", "Error happened : contact your administrator");
                }
                ViewBag.CategoryId = new SelectList(_db.Categories, "Id", "CategoryName", insider.CategoryId);
                return View(insider);
            }
        }

        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Insider insider = _db.Insiders.Find(id);
            if (insider == null)
            {
                return HttpNotFound();
            }

            ViewBag.CategoryId = new SelectList(_db.Categories, "Id", "CategoryName", insider.CategoryId);
            return View(insider);
        }

        // POST: Insiders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Login,FirstName,LastName,BirthSurname,Email,Role,CategoryId,Reason,Ceased,Obtained,CompanyNameAndAddress,Function,DateOfBirth,,SystemId,,WorkDirectTelephoneNumber,WorkMobileNumber,HomeTelephoneNumber,HomeMobileTelephoneNumber,AddressCountry,AddressCity,PostCode,AddressStreetName,AddressStreetNumber,NationalIdentificationNumber,UserRegistered")] Insider insider)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    insider.UserUpdated = DateTime.UtcNow;
                    _db.Entry(insider).State = EntityState.Modified;
                    _db.SaveChanges();
                    return RedirectToAction("InsidersList");
                }

                ViewBag.CategoryId = new SelectList(_db.Categories, "Id", "CategoryName", insider.CategoryId);
                return View(insider);
            }
            catch
            {
                var loginFromDb = _db.Insiders
                        .FirstOrDefault(ins => ins.Login.ToLower() == insider.Login.ToLower() && ins.Id != insider.Id);

                if (loginFromDb != null)
                {
                    ModelState.AddModelError("", "Error happened : the user with current login already exists");
                }
                else
                {
                    ModelState.AddModelError("", "Error happened : contact your administrator");
                }
                ViewBag.CategoryId = new SelectList(_db.Categories, "Id", "CategoryName", insider.CategoryId);
                return View(insider);
            }
        }

        // GET: Insiders/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Insider insider = _db.Insiders.Find(id);
            if (insider == null)
            {
                return HttpNotFound();
            }
            return View(insider);
        }

        // POST: Insiders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            try
            {
                Insider insider = _db.Insiders
                    .Include(ins => ins.EmailsCreated)
                    .Include(ins => ins.EmailsSended)
                    .FirstOrDefault(ins => ins.Id == id);

                _db.Insiders.Remove(insider);
                _db.SaveChanges();
                return RedirectToAction("InsidersList");
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
