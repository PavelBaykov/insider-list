﻿using InsiderList.DAL;
using InsiderList.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using InsiderList.Models;
using System.Net;

namespace InsiderList.Controllers
{
    [Authorize(Roles = "Assistant")]
    public class ReportsController : Controller
    {
        private InsidersContext _db = new InsidersContext();

        [HttpGet]
        public ActionResult ReportsList()
        {
            return View();
        }

        [HttpGet]
        public ActionResult MemberReport()
        {
            var emails = _db.Emails;

            ViewBag.EmailsId = new SelectList(emails, "Id", "Name");
            ViewBag.CategoriesId = new SelectList(_db.Categories, "Id", "CategoryName");

            return View();
        }

        [HttpPost]
        public ActionResult MemberReport(MemberReportViewModel model)
        {
            try
            {
                if (model.CategoriesId != null && model.Date.Year > 2000 && model.EmailsId != null && model.Status != null)
                {
                    return RedirectToAction("MemberReportViewer", new
                    {
                        emailId = model.EmailsId,
                        status = model.Status,
                        categoryId = model.CategoriesId,
                        date = model.Date
                    });
                }
                else if (model.CategoriesId != null && model.EmailsId != null && model.Status != null)
                {
                    return RedirectToAction("MemberReportViewer", new
                    {
                        emailId = model.EmailsId,
                        status = model.Status,
                        categoryId = model.CategoriesId,
                        date = DateTime.UtcNow
                    });
                }
                else
                {
                    throw new Exception("Exception");
                }
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        public ActionResult MemberReportViewer(string emailId, ConfirmationStatus status, string categoryId, DateTime date)
        {
            var confirmationsFromDb = _db.Confirmations
                .Include(conf => conf.Email)
                .Include(conf => conf.Insider)
                .Include(conf => conf.ConfirmationHistory)
                .Where(conf => conf.EmailId == emailId && conf.Insider.CategoryId == categoryId)
                .ToList();

            var suitableConfirmations = confirmationsFromDb
                .Where(conf => conf.ConfirmationHistory
                    .Any(hist => hist.EffectiveFrom <= date && hist.EffectiveTo >= date && hist.Status == status
                ));

            List<string> insiders = new List<string>();
            foreach (var confirmation in suitableConfirmations)
            {
                insiders.Add($"{confirmation.Insider.FirstName} {confirmation.Insider.LastName}");
            }

            List<string> insidersDistinct = insiders.Distinct().ToList();

            var cateogryName = _db.Categories.Find(categoryId).CategoryName;
            var emailName = _db.Emails.Find(emailId).Name;

            var model = new MemberReportViewerModel()
            {
                Date = date.ToString(),
                Category = cateogryName,
                Insiders = insidersDistinct,
                Name = emailName,
                Status = status.ToString()
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult EmailReport()
        {
            var emails = _db.Emails;

            ViewBag.EmailsId = new SelectList(emails, "Id", "Name");

            return View();
        }

        [HttpPost]
        public ActionResult EmailReport(string emailsId)
        {
            try
            {
                if (emailsId != null)
                {
                    return RedirectToAction("EmailReportViewer", new
                    {
                        emailId = emailsId
                    });
                }
                else
                {
                    return HttpNotFound();
                }
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        public ActionResult EmailReportViewer(string emailId)
        {
            var confirmationsFromDb = _db.Confirmations
                .Include(conf => conf.ConfirmationHistory)
                .Include(conf => conf.Email)
                .Include(conf => conf.Insider)
                .Where(conf => conf.EmailId == emailId)
                .ToList();

            if (confirmationsFromDb == null)
            {
                return HttpNotFound();
            }

            var emailFromDB = _db.Emails.Find(emailId);

            var model = new List<EmailReportViewerModel>();

            foreach (var conf in confirmationsFromDb)
            {
                var modelRow = new EmailReportViewerModel()
                {
                    Name = $"{ conf.Insider.FirstName} {conf.Insider.LastName}"
                };
                foreach (var hist in conf.ConfirmationHistory)
                {
                    if (hist.EffectiveTo.Year == 2040)
                    {
                        modelRow.Date = hist.Time.ToString();
                        modelRow.Ip = hist.Ip;
                        modelRow.Status = hist.Status.ToString();
                    }
                }
                model.Add(modelRow);
            }

            ViewBag.EmailName = emailFromDB?.Name;

            return View(model);
        }
    }

    public class MemberReportViewModel
    {
        public string EmailsId { get; set; }
        public string CategoriesId { get; set; }
        public ConfirmationStatus Status { get; set; }
        public DateTime Date { get; set; }
    }

    public class MemberReportViewerModel
    {
        public List<string> Insiders { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Status { get; set; }
        public string Date { get; set; }

    }

    public class EmailReportViewerModel
    {
        public string Name { get; set; }
        public string Status { get; set; }
        public string Date { get; set; }
        public string Ip { get; set; }
    }
}