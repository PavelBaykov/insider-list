﻿using InsiderList.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using InsiderList.Models;

namespace InsiderList.Controllers
{
    [Authorize(Roles = "Insider")]
    public class ConfirmationsController : Controller
    {
        private InsidersContext _db = new InsidersContext();
        [HttpGet]
        public ActionResult Confirmation(string confirmationId)
        {
            try
            {
                var confirmationFromDb = _db.Confirmations
                    .Include(conf => conf.Email)
                    .FirstOrDefault(conf => conf.Id == confirmationId);

                if (confirmationFromDb == null)
                {
                    return HttpNotFound();
                }

                var confirmationLink = $"{Url.Action("Confirm","Confirmations")}";
                var declineLink = $"{Url.Action("Decline", "Confirmations")}";
                var confirmationText = confirmationFromDb.Email.ConfirmationText + $"<p><a href=\"{confirmationLink}\">Confirm</a><a style=\"padding-left: 10px\" href=\"{declineLink}\">Reject</a></p>";
                var viewModel = new ConfirmationViewModel()
                {
                    Text = confirmationText
                };

                return View(viewModel);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        public ActionResult Confirm(string confirmationId)
        {
            try
            {
                var lastConfirmationHistoryFromDb = _db.ConfirmationsHistory
                    .Include(hist => hist.Confirmation)
                    .FirstOrDefault(hist => hist.Confirmation.Id == confirmationId && hist.EffectiveTo.Year == 2040);

                if (lastConfirmationHistoryFromDb == null)
                {
                    return HttpNotFound();
                }

                if (lastConfirmationHistoryFromDb.Status != ConfirmationStatus.Confirmed)
                {

                    lastConfirmationHistoryFromDb.EffectiveTo = DateTime.UtcNow;
                    var confirmationHistoryToInsert = new ConfirmationHistory()
                    {
                        Status = ConfirmationStatus.Confirmed,
                        EffectiveFrom = lastConfirmationHistoryFromDb.EffectiveTo.AddSeconds(1),
                        EffectiveTo = new DateTime(2040, 01, 01),
                        Time = DateTime.UtcNow,
                        Ip = Request.UserHostAddress,
                        ConfirmationId = confirmationId
                    };

                    _db.ConfirmationsHistory.Add(confirmationHistoryToInsert);
                    _db.SaveChanges();
                }

                return View("DataSaved");
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        public ActionResult Decline(string confirmationId)
        {
            try
            {
                var lastConfirmationHistoryFromDb = _db.ConfirmationsHistory
                    .Include(hist => hist.Confirmation)
                    .FirstOrDefault(hist => hist.Confirmation.Id == confirmationId && hist.EffectiveTo.Year == 2040);

                if (lastConfirmationHistoryFromDb == null)
                {
                    return HttpNotFound();
                }

                if (lastConfirmationHistoryFromDb.Status != ConfirmationStatus.Rejected)
                {

                    lastConfirmationHistoryFromDb.EffectiveTo = DateTime.UtcNow;
                    var confirmationHistoryToInsert = new ConfirmationHistory()
                    {
                        Status = ConfirmationStatus.Rejected,
                        EffectiveFrom = lastConfirmationHistoryFromDb.EffectiveTo.AddSeconds(1),
                        EffectiveTo = new DateTime(2040, 01, 01),
                        Time = DateTime.UtcNow,
                        Ip = Request.UserHostAddress,
                        ConfirmationId = confirmationId
                    };

                    _db.ConfirmationsHistory.Add(confirmationHistoryToInsert);
                    _db.SaveChanges();
                }

                return View("DataSaved");
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

    }



    public class ConfirmationViewModel
    {
        public string Text { get; set; }
    }

}