﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InsiderList.DAL;
using InsiderList.Models;
using InsiderList.Helpers;
using System.Net.Mail;

namespace InsiderList.Controllers
{
    [Authorize(Roles = "Assistant")]
    public class EmailsController : Controller
    {
        private InsidersContext _db = new InsidersContext();

        public ActionResult EmailsList()
        {
            var emails = _db.Emails
                .Include(e => e.InsiderSended)
            ;

            return View(emails.ToList());
        }

        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Email email = _db.Emails
                .Include(e => e.InsiderCreated)
                .Include(e => e.InsiderSended)
                .FirstOrDefault(e => e.Id == id);
            if (email == null)
            {
                return HttpNotFound();
            }
            return View(email);
        }

        public ActionResult Create()
        {
            return View();
        }

        // POST: Emails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,EmailSubject,EmailText,EmailFooter,ConfirmationText")] Email email)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userFromDb = InsiderHelpers.GetUserFromDb(User.Identity.Name);

                    email.InsiderCreatedId = userFromDb?.Id;
                    email.Status = EmailStatus.Created;
                    _db.Emails.Add(email);
                    _db.SaveChanges();
                    return RedirectToAction("EmailsList");
                }

                return View(email);
            }
            catch (Exception ex)
            {
                var emailFromDb = _db.Emails
                    .FirstOrDefault(em => em.Name == email.Name && em.Id != email.Id);

                if (emailFromDb != null)
                {
                    ModelState.AddModelError("", "Error happened: Email with this name already exists");
                }
                else
                {
                    ModelState.AddModelError("", "Error happened: contact your administrator");
                }
                return View(email);
            }
        }

        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Email email = _db.Emails.Find(id);
            if (email == null)
            {
                return HttpNotFound();
            }
            if (email.Status == EmailStatus.Sent)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return View(email);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,EmailSubject,EmailText,EmailFooter,ConfirmationText,InsiderCreatedId,Status,InsiderSendedId,DateSended,Confirmations")] Email email)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.Entry(email).State = EntityState.Modified;
                    _db.SaveChanges();
                    return RedirectToAction("EmailsList");
                }

                return View(email);
            }
            catch (Exception ex)
            {
                var emailFromDb = _db.Emails
                    .FirstOrDefault(em => em.Name == email.Name && em.Id != email.Id);

                if (emailFromDb != null)
                {
                    ModelState.AddModelError("", "Error happened: Email with this name already exists");
                }
                else
                {
                    ModelState.AddModelError("", "Error happened: contact your administrator");
                }
                return View(email);
            }
        }

        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Email email = _db.Emails
                .Include(e => e.InsiderCreated)
                .Include(e => e.InsiderSended)
                .FirstOrDefault(e => e.Id == id);

            if (email == null)
            {
                return HttpNotFound();
            }
            return View(email);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            try
            {
                Email email = _db.Emails.Find(id);
                _db.Emails.Remove(email);
                _db.SaveChanges();
                return RedirectToAction("EmailsList");
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpGet]
        public ActionResult SendEmail(string emailId)
        {
            var emailFromDb = _db.Emails
                .FirstOrDefault(e => e.Id == emailId);

            if (emailFromDb == null)
            {
                return HttpNotFound();
            }

            return View(emailFromDb);
        }

        [HttpPost]
        public ActionResult PostSendEmail(string emailId)
        {
            try
            {
                var emailFromDb = _db.Emails
                    .Include(e => e.Confirmations)
                    .FirstOrDefault(e => e.Id == emailId);

                if (emailFromDb == null)
                {
                    return HttpNotFound();
                }

                if (emailFromDb.Confirmations.Count > 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                }
                var userFromDb = InsiderHelpers.GetUserFromDb(User.Identity.Name);

                var insidersFromDbToSend = _db.Insiders
                    .Where(ins => ins.Role == Role.Insider);

                foreach (var ins in insidersFromDbToSend)
                {
                    var confirmationHistory = new ConfirmationHistory()
                    {
                        Status = ConfirmationStatus.Unknown,
                        EffectiveFrom = DateTime.Now,
                        EffectiveTo = new DateTime(2040, 01, 01)
                    };
                    var confirmation = new Confirmation()
                    {
                        InsiderId = ins.Id,
                        ConfirmationHistory = new List<ConfirmationHistory>() { confirmationHistory },
                        EmailId = emailFromDb.Id
                    };
                    _db.Confirmations.Add(confirmation);

                    var confirmationUrl = Url.Action("Confirmation", "Confirmations", new { confirmationId = confirmation.Id }, this.Request.Url.Scheme);
                    string body = $"{emailFromDb.EmailText}<p><a href='{confirmationUrl}'>Confirmation of access to inside information</a></p><p>{emailFromDb.EmailFooter}</p>";
                    SendEmailAction(emailFromDb.EmailSubject, body, ins.Email, $"{ins.FirstName} {ins.LastName}");
                }

                emailFromDb.Status = EmailStatus.Sent;
                emailFromDb.DateSended = DateTime.UtcNow;
                emailFromDb.InsiderSendedId = userFromDb.Id;

                _db.SaveChanges();

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        public string SendEmailAction(string subject, string body, string address, string displayName)
        {
            try
            {
                var emailAddress = System.Configuration.ConfigurationManager.AppSettings["EmailAddress"];
                var emailName = System.Configuration.ConfigurationManager.AppSettings["EmailName"];
                var emailPass = System.Configuration.ConfigurationManager.AppSettings["EmailPassword"];
                var fromAddress = new MailAddress(emailAddress, emailName);
                var toAddress = new MailAddress(address, displayName);

                var smtp = new SmtpClient
                {
                    Host = System.Configuration.ConfigurationManager.AppSettings["EmailHost"],
                    Port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EmailPort"]),
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, emailPass)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    IsBodyHtml = true,
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }

                return "sended";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }




    }
}
