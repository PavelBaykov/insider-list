﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.IdentityModel;
using System.IdentityModel.Services;
using Microsoft.AspNet.Identity;
using System.DirectoryServices.AccountManagement;
using System.IdentityModel.Tokens;


namespace InsiderList
{
    public class AppClaimsManager : ClaimsAuthenticationManager
    {
        public override ClaimsPrincipal Authenticate(string resourceName, ClaimsPrincipal incomingPrincipal)
        {
            if (!incomingPrincipal.Identity.IsAuthenticated)
            {
                return base.Authenticate(resourceName, incomingPrincipal);
            }

            ClaimsPrincipal appPrincipal = AddCustomClaimsToPrincipal(incomingPrincipal.Identity.Name);
            CreateSessionSecurityToken(appPrincipal);

            return base.Authenticate(resourceName, incomingPrincipal);
        } //</ public override ClaimsPrincipal Authenticate >

        //add application specific Claims to user's identity
        private ClaimsPrincipal AddCustomClaimsToPrincipal(String userName)
        {
            PrincipalContext princiContxt = null;
            UserPrincipal thePrincipal = null;

            //get the Domain context for the Directory Services
            princiContxt = new PrincipalContext(ContextType.Domain);

            //get the user-principal object from the Domain context using the specified username
            thePrincipal = UserPrincipal.FindByIdentity(princiContxt, userName);

            List<Claim> customClaims = new List<Claim>();
            customClaims.Add(new Claim(ClaimTypes.Surname, thePrincipal.Surname));
            customClaims.Add(new Claim(ClaimTypes.GivenName, thePrincipal.GivenName));


            //here you can add any claim type-value pairs, maybe some user settings read from DB.
            customClaims.Add(new Claim("myAppUserRole", "myAppAdmin"));
            customClaims.Add(new Claim("MyCustomClaim", "CustomClaimValue"));

            ClaimsIdentity theCustomClaimsIdentity = new ClaimsIdentity(customClaims, "Negotiate");
            return new ClaimsPrincipal(theCustomClaimsIdentity);
        }



        //create Session Security token and Write it to session cookie
        private void CreateSessionSecurityToken(ClaimsPrincipal appPrincipalToStoreInSession)
        {
            SessionSecurityToken sessionSecurityToken = new SessionSecurityToken(appPrincipalToStoreInSession, TimeSpan.FromHours(12));
            FederatedAuthentication.SessionAuthenticationModule.WriteSessionTokenToCookie(sessionSecurityToken);
        }
    }
}