namespace InsiderList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migration2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Insiders", "Obtained", c => c.DateTime());
            AddColumn("dbo.Insiders", "Ceased", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Insiders", "Ceased");
            DropColumn("dbo.Insiders", "Obtained");
        }
    }
}
