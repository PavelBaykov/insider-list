namespace InsiderList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        CategoryName = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.CategoryName, unique: true);
            
            CreateTable(
                "dbo.Insiders",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Login = c.String(nullable: false, maxLength: 50),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        BirthSurname = c.String(),
                        Email = c.String(nullable: false),
                        CategoryId = c.String(nullable: false, maxLength: 128),
                        Role = c.Int(nullable: false),
                        CompanyNameAndAddress = c.String(nullable: false, maxLength: 200),
                        Function = c.String(maxLength: 100),
                        NationalIdentificationNumber = c.String(),
                        WorkDirectTelephoneNumber = c.String(),
                        WorkMobileNumber = c.String(),
                        HomeTelephoneNumber = c.String(),
                        HomeMobileTelephoneNumber = c.String(),
                        DateOfBirth = c.DateTime(),
                        AddressCountry = c.String(),
                        AddressCity = c.String(),
                        PostCode = c.String(),
                        AddressStreetName = c.String(),
                        AddressStreetNumber = c.String(),
                        UserRegistered = c.DateTime(),
                        UserUpdated = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .Index(t => t.Login, unique: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Confirmations",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        InsiderId = c.String(nullable: false, maxLength: 128),
                        EmailId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Emails", t => t.EmailId, cascadeDelete: true)
                .ForeignKey("dbo.Insiders", t => t.InsiderId, cascadeDelete: true)
                .Index(t => t.InsiderId)
                .Index(t => t.EmailId);
            
            CreateTable(
                "dbo.ConfirmationHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ConfirmationId = c.String(nullable: false, maxLength: 128),
                        EffectiveFrom = c.DateTime(nullable: false),
                        EffectiveTo = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        Time = c.DateTime(),
                        Ip = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Confirmations", t => t.ConfirmationId, cascadeDelete: true)
                .Index(t => t.ConfirmationId);
            
            CreateTable(
                "dbo.Emails",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 50),
                        EmailSubject = c.String(nullable: false),
                        EmailText = c.String(nullable: false),
                        EmailFooter = c.String(nullable: false),
                        ConfirmationText = c.String(nullable: false),
                        Status = c.Int(nullable: false),
                        InsiderCreatedId = c.String(maxLength: 128),
                        InsiderSendedId = c.String(maxLength: 128),
                        DateSended = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Insiders", t => t.InsiderCreatedId)
                .ForeignKey("dbo.Insiders", t => t.InsiderSendedId)
                .Index(t => t.Name, unique: true, name: "IX_FirstAndSecond")
                .Index(t => t.InsiderCreatedId)
                .Index(t => t.InsiderSendedId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Insiders", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Emails", "InsiderSendedId", "dbo.Insiders");
            DropForeignKey("dbo.Emails", "InsiderCreatedId", "dbo.Insiders");
            DropForeignKey("dbo.Confirmations", "InsiderId", "dbo.Insiders");
            DropForeignKey("dbo.Confirmations", "EmailId", "dbo.Emails");
            DropForeignKey("dbo.ConfirmationHistories", "ConfirmationId", "dbo.Confirmations");
            DropIndex("dbo.Emails", new[] { "InsiderSendedId" });
            DropIndex("dbo.Emails", new[] { "InsiderCreatedId" });
            DropIndex("dbo.Emails", "IX_FirstAndSecond");
            DropIndex("dbo.ConfirmationHistories", new[] { "ConfirmationId" });
            DropIndex("dbo.Confirmations", new[] { "EmailId" });
            DropIndex("dbo.Confirmations", new[] { "InsiderId" });
            DropIndex("dbo.Insiders", new[] { "CategoryId" });
            DropIndex("dbo.Insiders", new[] { "Login" });
            DropIndex("dbo.Categories", new[] { "CategoryName" });
            DropTable("dbo.Emails");
            DropTable("dbo.ConfirmationHistories");
            DropTable("dbo.Confirmations");
            DropTable("dbo.Insiders");
            DropTable("dbo.Categories");
        }
    }
}
