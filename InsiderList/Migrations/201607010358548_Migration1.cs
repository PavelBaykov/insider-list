namespace InsiderList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migration1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Insiders", "Reason", c => c.String(maxLength: 500));
            AlterColumn("dbo.Insiders", "Function", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Insiders", "Function", c => c.String(maxLength: 100));
            DropColumn("dbo.Insiders", "Reason");
        }
    }
}
