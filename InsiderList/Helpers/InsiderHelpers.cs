﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InsiderList.DAL;
using InsiderList.Models;

namespace InsiderList.Helpers
{
    public static class InsiderHelpers
    {
        private static InsidersContext _db = new InsidersContext();

        public static Insider GetUserFromDb(string username)
        {
            var user = username.Substring(username.LastIndexOf('\\') + 1);

            var userFromDb = _db.Insiders
                .FirstOrDefault(ins => ins.Login.ToLower() == user);

            return userFromDb;
        }
    }
}