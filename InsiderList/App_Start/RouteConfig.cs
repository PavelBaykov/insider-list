﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace InsiderList
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "MemberReportViewer",
            //    url: "MemberReportViewer/{emailId}/{status}/{category}/{date}",
            //    defaults: new { controller = "Emails", action = "Decline" }
            //);

            routes.MapRoute(
                name: "Decline",
                url: "Decline/{confirmationId}",
                defaults: new { controller = "Confirmations", action = "Decline" }
            );

            routes.MapRoute(
                name: "Confirm",
                url: "Confirm/{confirmationId}",
                defaults: new { controller = "Confirmations", action = "Confirm" }
            );

            routes.MapRoute(
                name: "Confirmation",
                url: "Confirmation/{confirmationId}",
                defaults: new { controller = "Confirmations", action = "Confirmation" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Insiders", action = "InsidersList", id = UrlParameter.Optional }
            );
        }
    }
}
