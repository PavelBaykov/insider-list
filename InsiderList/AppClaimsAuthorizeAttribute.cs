﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Services;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace InsiderList
{
    public class AppClaimsAuthorizeAttribute : System.Web.Mvc.AuthorizeAttribute
    {
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }

        //Called when access is denied
        protected override void HandleUnauthorizedRequest(System.Web.Mvc.AuthorizationContext filterContext)
        {
            //User isn't logged in
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary(new { controller = "Home", action = "Index" })
                );
            }
            //User is logged in but has no access
            else
            {
                filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary(new { controller = "Error", action = "AccessDenied" })
                );
            }
        }



        //public override Task OnAuthorization(AuthorizationContext context) //, System.Threading.CancellationToken cancellationToken)
        //Core authentication, called before each action that is decoracted with authorize or appClaimsAuthorize attribute
        protected override bool AuthorizeCore(HttpContextBase context)
        {

            SessionSecurityToken token;
            ClaimsIdentity claimsIdentity = null;


            var isAuthorized = false;
            //try
            {
                claimsIdentity = context.User.Identity as ClaimsIdentity; // get current user's ClaimsIdentity (Widnow's identity as  ClaimsIdentity)
                isAuthorized = base.AuthorizeCore(context);

                if (!context.Request.IsAuthenticated || !isAuthorized)
                {
                    return false;
                }

                //check if the SessionSecurityToken is available in cookie, this will not be available during the first request to the application by a user.
                //if (FederatedAuthentication.SessionAuthenticationModule.TryReadSessionTokenFromCookie(out token))

                if (FederatedAuthentication.SessionAuthenticationModule != null && FederatedAuthentication.SessionAuthenticationModule.ContainsSessionTokenCookie(context.Request.Cookies))
                {
                    //var accessToken = await tokenManager.GetTokenFromStoreAsync(token.ClaimsPrincipal.Identity.Name);
                    FederatedAuthentication.SessionAuthenticationModule.TryReadSessionTokenFromCookie(out token);
                    claimsIdentity = token.ClaimsPrincipal.Identity as ClaimsIdentity;
                }
                else
                {
                    //else get the principal with Custom claims identity using CustomClaimsTransformer, which also sets it in cookie
                    ClaimsPrincipal currentPrincipal = context.User as ClaimsPrincipal; // ClaimsPrincipal.Current;
                    AppClaimsManager appClaimsMgr = new AppClaimsManager();
                    ClaimsPrincipal tranformedClaimsPrincipal = appClaimsMgr.Authenticate(string.Empty, currentPrincipal);
                    Thread.CurrentPrincipal = tranformedClaimsPrincipal;
                    HttpContext.Current.User = tranformedClaimsPrincipal;
                    claimsIdentity = tranformedClaimsPrincipal.Identities.First() as ClaimsIdentity;
                }


                isAuthorized = checkClaimValidity(claimsIdentity, ClaimType, ClaimValue);
            }
            //catch (Exception e)
            //{
            //    // Error handling code
            //    var exptnMsg = "error setting AuthorizeCore" + e.Message;
            //    return false;
            //}

            return isAuthorized;
        } // </ protected override bool AuthorizeCore >


        //checks Claim type/value in the given Claims Identity
        private Boolean checkClaimValidity(ClaimsIdentity pClaimsIdentity, string pClaimType, string pClaimValue)
        {
            Boolean blnClaimsValiditiy = false;
            //now check the passed in Claimtype has the passed in Claimvalue
            if (pClaimType != null && pClaimValue != null)
            {
                if ((pClaimsIdentity.HasClaim(x => x.Type.ToLower() == pClaimType.ToLower() && x.Value.ToLower() == pClaimValue.ToLower())))
                {
                    blnClaimsValiditiy = true;
                }
            }

            return blnClaimsValiditiy;

        }

    }
}